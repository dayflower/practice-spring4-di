package com.example.dayflower.practice.spring4.di;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@Slf4j
public class Main {

	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");

		SushiService sushiService = context.getBean(SushiService.class);

		log.info("寿司: {}", sushiService.getSushi());
	}

}
