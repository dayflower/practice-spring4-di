package com.example.dayflower.practice.spring4.di;

import java.util.List;

public interface FishService {
	public List<String> getFishes();
}
