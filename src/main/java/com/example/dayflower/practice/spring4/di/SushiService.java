package com.example.dayflower.practice.spring4.di;

import java.util.List;
import java.util.stream.Collectors;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SushiService {

	@Autowired @Getter @Setter
	private FishService fishService;

	@Autowired @Getter @Setter
	private RiceService riceService;

	public List<String> getSushi() {
		String rice = riceService.getRice();

		return fishService.getFishes().stream().map((fish) -> fish + ":" + rice).collect(Collectors.toList());
	}

}
