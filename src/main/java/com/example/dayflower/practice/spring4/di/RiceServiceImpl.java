package com.example.dayflower.practice.spring4.di;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RiceServiceImpl implements RiceService {

	@Autowired
	private KomedokoroServiceImpl komedokoroService;

	@Override
	public String getRice() {
		return komedokoroService.buildRice();
	}

}
