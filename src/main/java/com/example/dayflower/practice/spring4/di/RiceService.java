package com.example.dayflower.practice.spring4.di;

public interface RiceService {
	public String getRice();
}
