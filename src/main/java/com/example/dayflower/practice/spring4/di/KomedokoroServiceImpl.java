package com.example.dayflower.practice.spring4.di;

public class KomedokoroServiceImpl implements KomedokoroService {
	private final String name;

	public KomedokoroServiceImpl(String name) {
		this.name = name;
	}

	@Override
	public String buildRice() {
		return name + "のお米";
	}

}