package com.example.dayflower.practice.spring4.di;

import java.util.Arrays;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public class FishServiceImpl implements FishService {

	@Override
	public List<String> getFishes() {
		return Arrays.<String>asList("まぐろ", "いか", "たこ");
	}

}
